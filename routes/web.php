<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('page.welcome');
});


//-----------------------------Auth--------------------------------------------------------
Auth::routes();
Route::get('/profile/{username}/edit', 'UserController@edit');
Route::get('/profile/avatar', 'UserController@avatar');
Route::post('/profile/avatar/upload', 'UserController@avatar_store');
Route::put('/profile/{username}', 'UserController@update');
Route::get('/profile/{username}', 'UserController@show');
Route::get('/users', 'UserController@index');

Route::get('/profile/{username}/education/edit', 'UserEducationController@edit');
Route::post('/profile/education/store', 'UserEducationController@store');
Route::put('/profile/{username}/education/update', 'UserEducationController@update');

//-----------------------------Pages--------------------------------------------------------
Route::get('/home', 'HomeController@index');
Route::get('/contact', 'PageController@contact');
Route::get('/about', 'PageController@about');
Route::get('/search', 'PageController@search');


//------------------------------jobs--------------------------------------------------------

Route::get('/jobs', 'JobController@index');
Route::get('/jobs/{slug}', 'JobController@show');
Route::get('/jobs/{slug}/edit', 'JobController@edit');
Route::put('/jobs/{slug}', 'JobController@update');
Route::get('/jobs/location/{location}', 'JobController@category');
Route::get('/admin/jobs/create', 'JobController@create');
Route::post('/jobs', 'JobController@store');

//------------------------------Study--------------------------------------------------------
Route::get('/study', 'StudyController@menu');
Route::get('/admin/study/create', 'StudyController@create');
Route::post('/study', 'StudyController@store');
Route::get('/study/category/{category}', 'StudyController@index');
Route::get('/study/{slug}/', 'StudyController@show');
Route::get('/study/{slug}/edit', 'StudyController@edit');
Route::put('/study/{slug}/', 'StudyController@update');

Route::resource('/posts', 'PostsController');


//----------------------------------Sitemap--------------------------------------------------
Route::get('/sitemap.xml', 'SitemapController@index');
Route::get('/sitemap/jobs/sitemap.xml', 'SitemapController@Job');
Route::get('/sitemap/study/sitemap.xml', 'SitemapController@Study');
Route::get('/sitemap/posts/sitemap.xml', 'SitemapController@Post');
