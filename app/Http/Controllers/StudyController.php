<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Study;
use App\Models\User;
use Auth;

class StudyController extends Controller
{
    public function menu(){
        return view('study.menu');
    }

    public function index ($category){
        $category= Study::where('category', $category)->orderBy('id', 'desc')->paginate(20);
        return view('study.index',compact('category'));
    }

    public function show ($slug){
        $post= Study::where('slug', $slug)->first();
        return view('study.show',compact('post'));
    }

   public function create()
    {
            return view('study.create');
    }

    public function store(Request $request)
    {
        $slug = Str::slug($request->get('title'));
        Study::create([
            'user_id' => Auth::user()->id,
            'slug' => $slug,
            'category' => $request->category,
            'title' => $request->title,
            'content' => $request->content
        ]);
        return redirect('/study');
    }

    public function edit($id)
    {
        $post = Study::where('slug', $id)->firstOrFail();;
        if (Auth::user()->id == $post->user_id) {
            return view('study.edit',compact('post'));
        }

        return 'You can not edit this post';

    }

    public function update(Request $request, $id)
    {
        $post = Study::where('slug', $id)->firstOrFail();;
        $post->update($request->all());
        return redirect('/study/'.$post->slug);
    }

}
