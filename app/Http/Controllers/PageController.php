<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Job;
use App\Models\Study;
use App\Models\Post;

class PageController extends Controller
{
    public function about(){
        return view('page.about');
    }

    public function contact(){
        return view('page.contact');
    }

    public function search(Request $request) {

        if($request->cat == 'Study')
        {
            $category = Study::where('title', 'LIKE', '%'. $request->q .'%')
            ->orWhere("content", "LIKE", '%'. $request->q .'%')->orderBy('id', 'desc')->paginate(30);
            return view('study.index',compact('category'));
        }

        if($request->cat == 'Jobs')
        {
            $jobs = Job::where('title', 'LIKE', '%'. $request->q .'%')
            ->orWhere("content", "LIKE", '%'. $request->q .'%')->orderBy('id', 'desc')->paginate(30);
            return view('jobs.index', compact('jobs'));
        }

        if($request->cat == 'Discussion')
        {
            $posts = Post::where('title', 'LIKE', '%'. $request->q .'%')
            ->orWhere("content", "LIKE", '%'. $request->q .'%')->orderBy('id', 'desc')->paginate(30);
           return view('posts.index', compact('posts'));
        }
    }
}
