<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Job;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Auth;

class JobController extends Controller
{
    public function index() {
       $jobs= Job::orderBy('id', 'desc')->paginate(200);
       return view('jobs.index', compact('jobs'));
    }
    public function show($slug) {
        $job= Job::where('slug', $slug)->firstOrFail();
        return view('jobs.show', compact('job'));
    }

    public function category($location) {
       $jobs = Job::where('location', $location)->orderBy('id', 'desc')->paginate(100);
       return view('jobs.index', compact('jobs'));
    }

    public function create()
    {
            return view('jobs.create');
    }

    public function store(Request $request)
    {
        $slug = Str::slug($request->get('title'));
        Job::create([
            'user_id' => Auth::user()->id,
            'slug' => $slug,
            'title' => $request->title,
            'job_position' => $request->job_position,
            'location' => $request->location,
            'company' => $request->company,
            'eligibility' => $request->eligibility,
            'experience' => $request->experience,
            'content' => $request->content,
        ]);
        return redirect('/jobs');
    }


    public function edit($id)
    {
        $job = Job::where('slug', $id)->firstOrFail();;
        if (Auth::user()->id == $job->user_id) {
            return view('jobs.edit',compact('job'));
        }

        return 'You can not edit this post';

    }

    public function update(Request $request, $id)
    {
        $job = Job::where('slug', $id)->firstOrFail();;
        $job->update($request->all());
        return redirect('/jobs/'.$job->slug);
    }
}
