<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Image;
use App\Models\Usereducation;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $users = User::all();
        return view('users.index',compact('users'));
    }

    public function show($username){
        $user = User::where('username', $username)->firstorFail();
        $usereducation = Usereducation::where('user_id', $user->id)->first();
        return view('users.show',compact('user','usereducation'));
    }

    public function edit($username){
        $user = User::where('username', $username)->firstOrFail();

        if (Auth::user()->id == $user->id) {
            return view('auth.edit',compact('user'));
        }

        return 'You can not edit your profile';

    }

    public function update(Request $request, $username){
        $user = User::where('username', $username)->firstOrFail();;
        $user->update($request->all());
        return redirect('/home');
    }

    public function profile($username){
        $user = User::where('username', $username)->firstOrFail();;
        return 'found';
    }

   public function avatar(Request $request)
	{
       return "<form action'/profile/avatar' method='POST'>". csrf_field() ."<input type='file' name='image'><input type='submit' value='Upload'></form> ";
	}

    public function avatar_store(Request $request)
	{
        $user = Auth::user();
        $username = Auth::user()->username;

        if($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $file = time() . '.'.$avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(500,500)->save(public_path().'/images/'.$username.'_'. $file);
            $user->avatar = $username.'_'.$file;
            $user->save();
        }

        return redirect('/home');
    }

}
