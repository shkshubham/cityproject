<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Str;

use Auth;
use App\Models\Post;
use App\Models\User;

class PostsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|max:255',
            'content' => 'required|email|max:255|unique',
            'tags' => 'required|min:6|unique',
            'slug' => 'unique'
        ]);
    }


    public function index()
    {
        $posts= Post::orderBy('id', 'desc')->paginate(30);
        return view('posts.index', compact('posts'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        $slug = Str::slug($request->get('title'));
        Post::create([
            'user_id' => Auth::user()->id,
            'slug' => $slug,
            'tags' => $request->tags,
            'title' => $request->title,
            'content' => $request->content
        ]);
        return redirect('/posts');
    }

    public function show($id)
    {
        $post = Post::where('slug', $id)->firstOrFail();;
        return view('posts.show',compact('post'));
    }

    public function edit($id)
    {
        $post = Post::where('slug', $id)->firstOrFail();
        if (Auth::user()->id == $post->user_id) {
            return view('posts.edit',compact('post'));
        }

        return 'You can not edit this post';

    }

    public function update(Request $request, $id)
    {
        $post = Post::where('slug', $id)->firstOrFail();
        $post->update($request->all());
        return redirect('/posts/'.$post->slug);
    }

    public function destroy($id)
    {
        $post = Post::where('slug', $id)->first();
        
        return $post;
    }

    public function getShortContentAttribute(){
        return substr($this->content, 0, random_init(65, 180)) . '...';
    }
}
