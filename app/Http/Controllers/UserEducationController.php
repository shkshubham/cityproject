<?php

namespace App\Http\Controllers;

use App\Models\Usereducation;
use Auth;
use App\Models\User;

use Illuminate\Http\Request;

class UserEducationController extends Controller
{
    public function edit($username){

        $usereducation = Usereducation::where('user_id', Auth::user()->id)->first();
        if ($usereducation) {
            return view('users.education.edit',compact('usereducation'));
        }

        return view('users.education.create');

    }

    public function store(Request $request){

        Usereducation::create([
            'user_id' => Auth::user()->id,
            'school_10' => $request->school_10,
            'location_10' => $request->location_10,
            'year_completed_10' => $request->year_completed_10,
            'aggregate_10' => $request->aggregate_10,
            'board_10' => $request->board_10,
            'grade_10' => $request->grade_10,
            //12th
            'school_12' => $request->school_12,
            'location_12' => $request->location_12,
            'year_completed_12' => $request->year_completed_12,
            'aggregate_12' => $request->aggregate_12,
            'board_12' => $request->board_12,
            'grade_12' => $request->grade_12,
            //Graduation
            'grad_course_type' => $request->grad_course_type,
            'grad_degree' => $request->grad_degree,
            'grad_university' => $request->grad_university,
            'grad_college' => $request->grad_college,
            'grad_specialization' => $request->grad_specialization,
            'grad_start_date' => $request->grad_start_date,
            'grad_end_date' => $request->grad_end_date,
            'grad_prefinal_aggrigate' => $request->grad_prefinal_aggrigate,
            'grad_final_aggrigate' => $request->grad_final_aggrigate,
            'grad_grade' => $request->grad_grade,
        ]);
        return redirect('/home');
    }

    public function update(Request $request, $username){
        $user = User::where('username', $username)->firstOrFail();
        $usereducation = Usereducation::where('user_id', $user->id)->firstOrFail();
        $usereducation->update($request->all());
        return redirect('/home');
    }
}
