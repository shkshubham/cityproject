<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Job;
use App\Models\Study;
use App\Models\Post;

class SitemapController extends Controller
{
     public function Study(){
            $study=Study::orderBy('id', 'desc')->get();
            return response()->view('page.sitemap.study', compact('study'))->header('Content-Type', 'text/xml');
    }

    public function Job(){
            $jobs= Job::orderBy('id', 'desc')->get();
            return response()->view('page.sitemap.job', compact('jobs'))->header('Content-Type', 'text/xml');
    }

    public function index(){
            return response()->view('page.sitemap.index')->header('Content-Type', 'text/xml');
    }

    public function Post(){
            $posts= Post::orderBy('id', 'desc')->get();
            return response()->view('page.sitemap.post', compact('posts'))->header('Content-Type', 'text/xml');
    }
}