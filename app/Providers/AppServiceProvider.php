<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\Job;
use App\Models\Study;
use App\Models\Post;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		Schema::defaultStringLength(191);
        view()->composer('*', function ($view) {
            $view->with('GlobalJob', Job::orderBy('id', 'desc')->simplepaginate(5));
        });

        view()->composer('*', function ($view) {
            $view->with('GlobalStudy', Study::orderBy('id', 'desc')->simplepaginate(5));
        });

        view()->composer('*', function ($view) {
            $view->with('GlobalPost', Post::orderBy('id', 'desc')->simplepaginate(5));
        });
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
