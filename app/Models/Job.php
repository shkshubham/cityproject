<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{

 protected $fillable = [
        'title', 'content', 'location', 'user_id','slug','job_position','eligibility','experience','company',
    ];

    protected $hidden = [
        'user_id', 'remember_token',
    ];
}
