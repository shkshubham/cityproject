<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Study extends Model
{
        protected $fillable = [
        'title', 'content', 'category', 'user_id','slug',
    ];

    protected $hidden = [
        'user_id', 'remember_token',
    ];
}
