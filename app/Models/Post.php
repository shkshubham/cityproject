<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title', 'content', 'tags', 'user_id','slug',
    ];

    protected $hidden = [
        'user_id', 'remember_token',
    ];
}
