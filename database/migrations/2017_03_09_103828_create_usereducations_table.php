<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsereducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usereducations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            //10th detail
            $table->string('school_10');
            $table->string('location_10');
            $table->string('year_completed_10');
            $table->string('board_10');
            $table->string('aggregate_10');
            $table->string('grade_10');

            //12 details
            $table->string('school_12');
            $table->string('location_12');
            $table->string('year_completed_12');
            $table->string('board_12');
            $table->string('aggregate_12');
            $table->string('grade_12');

            //Graduation Detail
            $table->string('grad_course_type');
            $table->string('grad_degree');
            $table->string('grad_university');
            $table->string('grad_college');
            $table->string('grad_specialization');
            $table->string('grad_start_date');
            $table->string('grad_end_date');
            $table->string('grad_prefinal_aggrigate');
            $table->string('grad_final_aggrigate');
            $table->string('grad_grade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usereducations');
    }
}
