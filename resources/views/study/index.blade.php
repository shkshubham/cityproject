@extends('layout.app')

@section('title')

{{ 'Study' . ' |'}}
@endsection


@section('style')
.ReadMore{
    margin-bottom:10px;
}
.glyphicon { margin-right:5px; }
.thumbnail
{
    margin-bottom: 20px;
    padding: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
}

.item.list-group-item
{
    float: none;
    width: 100%;
    background-color: #fff;
    margin-bottom: 10px;
}
.item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
{
    background: #428bca;
}

.item.list-group-item .list-group-image
{
    margin-right: 10px;
}
.item.list-group-item .thumbnail
{
    margin-bottom: 0px;
}
.item.list-group-item .caption
{
    padding: 9px 9px 0px 9px;
}
.item.list-group-item:nth-of-type(odd)
{
    background: #eeeeee;
}

.item.list-group-item:before, .item.list-group-item:after
{
    display: table;
    content: " ";
}

.item.list-group-item img
{
    float: left;
    height:224px;
    width:400px;
}
.item.list-group-item:after
{
    clear: both;
}
.list-group-item-text
{
    margin: 0 0 11px;
}


@endsection

@section('header')
    <div class="intro-header"style="background-color: rgb(240,95,64)">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1>ELC | LEARNING</h1>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('content')
<div class="container">
    <div class="well well-sm">
        <strong>Post</strong>
    </div>
<div id="products" class="row list-group">
     @foreach($category as $cat)
        <div class="item  col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="thumbnail">
                <img class="group list-group-image" @if($cat->category == 'Quantitative')
                                                    src="/img/default/section/quantitative.jpg"
                                                    @elseif($cat->category == 'Verbal')
                                                    src="/img/default/section/verbal.jpg"
                                                    @elseif($cat->category == 'Reasoning')
                                                    src="/img/default/section/reasoning.jpg"
                                                    @endif

                                                     alt="" />
                <div class="caption">
                    <h4 class="group inner list-group-item-heading">
                        {{ $cat->title }} | <small>{{ $cat->created_at->toFormattedDateString() }}</small></h4>
                    <p class="group inner list-group-item-text">
                        {!! substr($cat->content,0,90) !!}</p>
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <a class="ReadMore btn btn-success pull-right" href="/study/{{ $cat->slug}}/">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endforeach
    </div>
            {{ $category->links() }}
            </div>
        </div>
@endsection

@section('script')
@endsection
