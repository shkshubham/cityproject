@extends('layout.app')

@section('header')
@include('include.header')

@endsection
@section('content')
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Create Post</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/study">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" placeholder='Title' required autofocus>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                  <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <textarea id="content" class="form-control" name="content" value="{{ old('content') }}"></textarea>

                                @if ($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                            <label for="category" class="col-md-1">Category</label>
                            <div class="col-md-12">
                                <select id="category" name="category" class="form-control" value="{{ old('category') }}" required>
                                    <option value="Quantitative">Quantitative</option>
                                    <option value="Verbal">Verbal</option>
                                    <option value="Reasoning">Logical Reasoning</option>
                                    <option value="Technical">Technical Skills</option>
                                 </select>
                                @if ($errors->has('category'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    Post
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection

@section('sidebar')
 @include('include.sidebar')
@endsection

@section('script')
@include('include.tinymce')
 </script>
@endsection