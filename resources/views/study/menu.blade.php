@extends('layout.app')

@section('style')

.glyphicon { margin-right:5px; }
.thumbnail
{
    margin-bottom: 20px;
    padding: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
}

.item.list-group-item
{
    float: none;
    width: 100%;
    background-color: #fff;
    margin-bottom: 10px;
}
.item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
{
    background: #428bca;
}

.item.list-group-item .list-group-image
{
    margin-right: 10px;
}
.item.list-group-item .thumbnail
{
    margin-bottom: 0px;
}
.item.list-group-item .caption
{
    padding: 9px 9px 0px 9px;
}
.item.list-group-item:nth-of-type(odd)
{
    background: #eeeeee;
}

.item.list-group-item:before, .item.list-group-item:after
{
    display: table;
    content: " ";
}

.item.list-group-item img
{
    float: left;
}
.item.list-group-item:after
{
    clear: both;
}
.list-group-item-text
{
    margin: 0 0 11px;
}

@endsection

@section('title')

{{ 'Study' . ' |'}}
@endsection

@section('header')
    <div class="intro-header" style="background-color: rgb(51,62,80)">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1>ELC | LEARNING</h1>
                        <hr class="small">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('content')


<div class="container">
    <div class="well well-sm">
        <strong>Category</strong>
    </div>
    <div id="products" class="row list-group">
        <div class="item  col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="thumbnail">
                <img class="group list-group-image" src="/img/default/section/verbal.jpg" alt="" />
                <div class="caption">
                    <h4 class="group inner list-group-item-heading">
                        Verbal</h4>
                    <p class="group inner list-group-item-text">
                        Verbal Ability Tutorial for Beginners - Learn Verbal Ability in simple and
                        easy steps starting from basic to advanced concepts with examples.</p>
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <a class="btn btn-success pull-right" href="/study/category/Verbal/">More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item  col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="thumbnail">
                <img class="group list-group-image" src="/img/default/section/quantitative.jpg" alt="" />
                <div class="caption">
                    <h4 class="group inner list-group-item-heading">
                        Quantitative</h4>
                    <p class="group inner list-group-item-text">
                        Quantitative Tutorial for Beginners - Learn Quantitative in simple and
                        easy steps starting from basic to advanced concepts with examples.</p>
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <a class="btn btn-success pull-right" href="/study/category/Quantitative/">More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item  col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="thumbnail">
                <img class="group list-group-image" src="/img/default/section/reasoning.jpg" alt="" />
                <div class="caption">
                    <h4 class="group inner list-group-item-heading">
                        Logical Reasoning</h4>
                    <p class="group inner list-group-item-text">
                        Logical Reasoning Tutorial for Beginners - Learn Logical Reasoning in simple and
                        easy steps starting from basic to advanced concepts with examples.
                    </p>
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <a class="btn btn-success pull-right" href="/study/category/Reasoning/">More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

            <div class="item  col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="thumbnail">
                <img class="group list-group-image" src="/img/default/section/technical.jpg" alt="" />
                <div class="caption">
                    <h4 class="group inner list-group-item-heading">
                        Technical Skills</h4>
                    <p class="group inner list-group-item-text">
                        Technical Skills Tutorial for Beginners - Learn Technical Skills in simple and
                        easy steps starting from basic to advanced concepts with examples.
                    </p>
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <a class="btn btn-success pull-right" href="/study/category/Technical/">More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
@endsection

@section('script')

@endsection
