@extends('layout.app')

@section('header')
        <div class="intro-header" style="background-color: rgb(240,95,64)">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>WELCOME</h1>
                        <hr class="small">
                        <h2 class='subheading'>{{ Auth::user()->name}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
#upload-file-selector {
    display:none;
}
/*  bhoechie tab */
div.bhoechie-tab-container{
  z-index: 10;
  background-color: #ffffff;
  padding: 0 !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  border:1px solid #ddd;

  background-clip: padding-box;
  opacity: 0.97;
  filter: alpha(opacity=97);
}
div.bhoechie-tab-menu{
  padding-right: 0;
  padding-left: 0;
  padding-bottom: 0;
}
div.bhoechie-tab-menu ul.list-group{
  margin-bottom: 0;
  list-style:none;
}
div.bhoechie-tab-menu ul.list-group>a{
  margin-bottom: 0;
  text-align:left;
}
div.bhoechie-tab-menu ul.list-group>a .glyphicon,
div.bhoechie-tab-menu ul.list-group>a .fa {
  color: #00001a;
}
div.bhoechie-tab-menu ul.list-group>a:first-child{
  border-top-right-radius: 0;
  -moz-border-top-right-radius: 0;
}
div.bhoechie-tab-menu ul.list-group>a:last-child{
  border-bottom-right-radius: 0;
  -moz-border-bottom-right-radius: 0;
}
div.bhoechie-tab-menu ul.list-group>a.active,
div.bhoechie-tab-menu ul.list-group>a.active .glyphicon,
div.bhoechie-tab-menu ul.list-group>a.active .fa{
  background-color: #004c99;
  background-image: #5A55A3;
  color: #ffffff;
}
div.bhoechie-tab-menu ul.list-group>a.active:after{
  content: '';
  position: absolute;
  left: 100%;
  top: 50%;
  margin-top: -13px;
  border-left: 0;
  border-bottom: 13px solid transparent;
  border-top: 13px solid transparent;
  border-left: 10px solid #5A55A3;
}

div.bhoechie-tab-content{
  background-color: #ffffff;
  /* border: 1px solid #eeeeee; */
  padding-left: 20px;
  padding-top: 10px;
}

div.bhoechie-tab div.bhoechie-tab-content:not(.active){
  display: none;
}
@endsection

@section('content')

    <div class='container-fuild'>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 bhoechie-tab-menu">
                <ul class="list-group">
                    <a href="#" class="list-group-item active">
                        <br/><br/><i class="glyphicon glyphicon-home"></i> Home<br/><br/>
                    </a>
                    <a href="#" class="list-group-item ">
                        <br/><br/><i class="glyphicon glyphicon-tasks"></i> Profile<br/><br/>
                    </a>
                    <a href="#" class="list-group-item ">
                        <br/><br/><i class="glyphicon glyphicon-transfer"></i> Education<br/><br/>
                    </a>
                </ul>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 bhoechie-tab">
                <!-- flight section -->
                <div class="bhoechie-tab-content active">
                    <center>
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " align="center">
                            <h2>{{Auth::user()->name}}</h2>
                            <h5>{{Auth::user()->username}}</h5>
                            <img alt="User Pic" src="/images/{{Auth::user()->avatar}}" height='300px' width='300px' class="img-circle img-responsive">

                            <form enctype="multipart/form-data" class="form-horizontal" role="form" method="POST" action="/profile/avatar/upload">
                                {{ csrf_field() }}
                                <span id="file_selector">
                                    <label class="btn btn-default" for="upload-file-selector">
                                        <input id="upload-file-selector" type="file" name='avatar'>
                                        <i class="fa_icon"></i>Select Pic
                                    </label>
                                </span>
                                <br>
                                <button type="submit" class="btn btn-primary">
                                    Edit Photo
                                </button>
                            </form>
                        </div>
                    </center>
                </div>


                <div class="bhoechie-tab-content">
                    <center>
                        @include('users.show.general')
                    </center>
                </div>


                <div class="bhoechie-tab-content">
                    <center>
                        @if($usereducation)
                            @include('users.show.education')
                        @else
                            <p>Please Fill your Education Details</p><a href='/profile/{{Auth::user()->username}}/education/edit'>Click here</a>
                        @endif
                    </center>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
$(document).ready(function() {
    $("div.bhoechie-tab-menu>ul.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});
</script>
@endsection
