@extends('layout.app')



@section('title')

{{'About US' . ' |'}}
@endsection

@section('style')
@import url(https://fonts.googleapis.com/css?family=Quicksand:400,300);
body{
    font-family: 'Quicksand', sans-serif;
}
.team{
    padding:75px 0;
}
h6.description{
	font-weight: bold;
	letter-spacing: 2px;
	color: #999;
	border-bottom: 1px solid rgba(0, 0, 0,0.1);
	padding-bottom: 5px;
}
.profile{
	margin-top: 25px;
}
.profile h1{
	font-weight: normal;
	font-size: 20px;
	margin:10px 0 0 0;
}
.profile h2{
	font-size: 14px;
	font-weight: lighter;
	margin-top: 5px;
}
.profile .img-box{
	opacity: 1;
	display: block;
	position: relative;
}
.profile .img-box:after{
	content:"";
	opacity: 0;
	background-color: rgba(0, 0, 0, 0.75);
	position: absolute;
	right: 0;
	left: 0;
	top: 0;
	bottom: 0;
}
.img-box ul{
	position: absolute;
	z-index: 2;
	bottom: 50px;
	text-align: center;
	width: 100%;
	padding-left: 0px;
	height: 0px;
	margin:0px;
	opacity: 0;
}
.profile .img-box:after, .img-box ul, .img-box ul li{
	-webkit-transition: all 0.5s ease-in-out 0s;
    -moz-transition: all 0.5s ease-in-out 0s;
    transition: all 0.5s ease-in-out 0s;
}
.img-box ul i{
	font-size: 20px;
	letter-spacing: 10px;
}
.img-box ul li{
	width: 30px;
    height: 30px;
    text-align: center;
    border: 1px solid #F05F40;
    margin: 2px;
    padding: 5px;
	display: inline-block;
}
.img-box a{
	color:#fff;
}
.img-box:hover:after{
	opacity: 1;
}
.img-box:hover ul{
	opacity: 1;
}
.img-box ul a{
	-webkit-transition: all 0.3s ease-in-out 0s;
	-moz-transition: all 0.3s ease-in-out 0s;
	transition: all 0.3s ease-in-out 0s;
}
.img-box a:hover li{
	border-color: #fff;
	color: #F05F40;
}


i.red{
    color:#BC0213;
}
@endsection

@section('header')
        <div class="intro-header" style="background-image: url('/theme/img/about-bg.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>About US</h1>
                        <hr class="small">
                        <span class="subheading">This is what we do.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('content')
<section class="team">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="col-lg-12">
          <h6 class="description">OUR TEAM</h6>
          <div class="row pt-md">

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
              <div class="img-box">
                <img src="/img/team/16601870_1237276396320603_4368129379698243426_o.jpg" class="img-responsive">
                <ul class="text-center">
                  <a href="https://www.facebook.com/ashish.bisht.saint"><li><i class="fa fa-facebook"></i></li></a>
                </ul>
              </div>
              <h1>Ashish Bisht</h1>
              <h2>Co-founder/ Operations</h2>

            </div>

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
              <div class="img-box">
                <img src="/img/team/16826024_1080579675418729_6020276741323019810_o.jpg" class="img-responsive">
                <ul class="text-center">
                  <a href="https://www.facebook.com/gauravsingh.rawat.161"><li><i class="fa fa-facebook"></i></li></a>
                </ul>
              </div>
              <h1>Gaurav Singh Rawat</h1>
              <h2>Co-founder/ Marketing</h2>

            </div>

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
              <div class="img-box">
                <img src="/img/team/15591649_1353264094705845_4922354658272516565_o.jpg" class="img-responsive">
                <ul class="text-center">
                  <a href="https://www.facebook.com/rajats105"><li><i class="fa fa-facebook"></i></li></a>
                </ul>
              </div>
              <h1>Rajat Shukla</h1>
              <h2>Co-founder/ Projects</h2>

            </div>

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
              <div class="img-box">
                <img src="/img/team/14435067_1237895859617623_7147032224611763937_o.jpg" class="img-responsive">
                <ul class="text-center">
                  <a href="https://www.facebook.com/greedshukla"><li><i class="fa fa-facebook"></i></li></a>
                </ul>
              </div>
              <h1>Shubham Shukla</h1>
              <h2>Web Developer</h2>

            </div>
                      </div>
        </div>
      </div>
    </div>
  </div>
</section>
<footer>
    <div class="container">
        <div class="col-md-10 col-md-offset-1 text-center">
            The team consists of highly motivated individuals looking forward to help the job seekers across the country by providing them a platform where they can connect with each other to learn new things and enhance their knowledge to get placed into their dream companies. We are constantly updating our content so that you can get regular info about various Job openings and prepare for them by referring our study section and discussion forum.
            <h6>Coded with <i class="fa fa-heart red"></i> love</h6>
        </div>
    </div>
</footer>
@endsection

@section('script')

@endsection
