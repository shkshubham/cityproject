<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($study as $stud)
        <url>
            <loc>https://www.citywalkin.com/study/{{ $stud->slug }}</loc>
        </url>
    @endforeach
</urlset>
