<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    @foreach($jobs as $job)
        <url>
            <loc>https://www.citywalkin.com/jobs/{{ $job->slug }}</loc>
        </url>
    @endforeach
</urlset>
