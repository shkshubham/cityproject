<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>https://www.citywalkin.com/</loc>
    </url>
    <url>
        <loc>https://www.citywalkin.com/study</loc>
    </url>
    <url>
        <loc>https://www.citywalkin.com/jobs</loc>
    </url>
    <url>
        <loc>https://www.citywalkin.com/posts</loc>
    </url>
    <url>
        <loc>https://www.citywalkin.com/login</loc>
    </url>
    <url>
        <loc>https://www.citywalkin.com/register</loc>
    </url>
    <url>
        <loc>https://www.citywalkin.com/contact</loc>
    </url>
    <url>
        <loc>https://www.citywalkin.com/about</loc>
    </url>
</urlset>
