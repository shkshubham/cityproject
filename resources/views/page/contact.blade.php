@extends('layout.app')



@section('title')

{{'Contact US' . ' |'}}
@endsection

@section('style')
.first-box{padding:10px;background:#9C0;}
.second-box{padding:10px; background:#39F;}
.third-box{padding:10px;background:#F66;}
.fourth-box{padding:10px;background:#6CC;}
@endsection

@section('header')
        <div class="intro-header" style="background-image: url('/theme/img/about-bg.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Contact US</h1>
                        <hr class="small">
                        <span class="subheading">Feel free to contact us.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('content')
<div class="container">
<h1>Contact Address</h1><br>
	<div class="row text-center">
		<div class="col-sm-3 col-xs-6 first-box">
        <h1><span class="glyphicon glyphicon-earphone"></span></h1>
        <h3>Phone</h3>
        <p>+91-7447667795</p><br>
    </div>
    <div class="col-sm-3 col-xs-6 second-box">
        <h1><span class="glyphicon glyphicon-home"></span></h1>
        <h3>Location</h3>
        <p>Pune</p><br>
    </div>
    <div class="col-sm-3 col-xs-6 third-box">
        <h1><span class="glyphicon glyphicon-send"></span></h1>
        <h3>E-mail</h3>
        <p>citywalkin@eutaxy.in</p><br>
    </div>
    <div class="col-sm-3 col-xs-6 fourth-box">
    	<h1><span class="glyphicon glyphicon-leaf"></span></h1>
        <h3>Web</h3>
        <p>www.CityWalkIn.com</p><br>
    </div>
	</div>
</div>
@endsection

@section('script')

@endsection
