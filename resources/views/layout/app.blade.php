<!doctype html>
<html lang="en">
    <head>
        @include('include.head')
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-5405157603871625",
    enable_page_level_ads: true
  });
</script>
<meta name="google-site-verification" content="nMYZpJqhQiu1YpJtekPcWYfVulWnha-xxtdFU7IEtVI" />
<meta name="msvalidate.01" content="7F7A2B427660BB1E0525DE5243F7FC81" />
        <style>@yield('style')</style>

        <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>
    </head>

    <body>
        @include('include.analyticstracking')

        @include('include.navbar')

        <!-- @include('include.header') -->

        @yield('header')

        <!-- Page Content -->
        <div class="container-fluid">
            @yield('content')

            <div class="col-lg-4 col-md-4">
                @yield('adsense')
                @yield('sidebar')
            </div>
        </div>

        @include('include.footer')

        @include('include.bottom')

        @yield('script')
    </body>
</html>
