@extends('layout.app')

@section('title')

{{ 'Jobs' . ' |'}}
@endsection


@section('style')
.filterable {
    margin-top: 15px;
}
.filterable .panel-heading .pull-right {
    margin-top: -20px;
}
.filterable .filters input[disabled] {
    background-color: transparent;
    border: none;
    cursor: auto;
    box-shadow: none;
    padding: 0;
    height: auto;
}
.filterable .filters input[disabled]::-webkit-input-placeholder {
    color: #333;
}
.filterable .filters input[disabled]::-moz-placeholder {
    color: #333;
}
.filterable .filters input[disabled]:-ms-input-placeholder {
    color: #333;
}


@endsection

@section('header')
    <div class="intro-header" style="background-color: rgb(1,176,241)">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1>CityWalkIn | Jobs</h1>
                        <hr class="small">
                        <span class="subheading">Search Jobs</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('content')
<div class="col-lg-12 col-md-12">
       <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <h3 class="panel-title">Jobs</h3>
                <div class="pull-right">
                    <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                </div>
            </div>
            <div class="table-responsive">
            <table class="table table-hover table-bordered table-striped">
                <thead>
                    <tr class="filters">
                        <th><input type="text" class="form-control" placeholder="Post Date" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Job Position" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Company" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Location" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Eligibility" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Experience" disabled></th>
                        <th><input type="text" class="form-control" placeholder="More Info" disabled></th>

                    </tr>
                </thead>
                <tbody>
                 @foreach($jobs as $job)
                    <tr>
                        <td>{{ $job->created_at->toFormattedDateString() }}</td>
                        <td>{{ $job->job_position }} </td>
                        <td>{{ $job->company }}</td>
                        <td><a href="/jobs/location/{{ $job->location}}/">{{ $job->location }}</a></td>
                        <td>{{ $job->eligibility }}</td>
                        <td>{{ $job->experience }}</td>
                        <td><a href="/jobs/{{ $job->slug}}" class="btn btn-primary btn-lg" role="button">Detail</a></td>
                    </tr>
                    @endforeach
                </tbody>
                     </table>
                     {{ $jobs->links() }}
                     </div>
        </div>
          
    </div>


</div>

@endsection

@section('script')
<script>
$(document).ready(function(){
    $('.filterable .btn-filter').click(function(){
        var $panel = $(this).parents('.filterable'),
        $filters = $panel.find('.filters input'),
        $tbody = $panel.find('.table tbody');
        if ($filters.prop('disabled') == true) {
            $filters.prop('disabled', false);
            $filters.first().focus();
        } else {
            $filters.val('').prop('disabled', true);
            $tbody.find('.no-result').remove();
            $tbody.find('tr').show();
        }
    });

    $('.filterable .filters input').keyup(function(e){
        /* Ignore tab key */
        var code = e.keyCode || e.which;
        if (code == '9') return;
        /* Useful DOM data and selectors */
        var $input = $(this),
        inputContent = $input.val().toLowerCase(),
        $panel = $input.parents('.filterable'),
        column = $panel.find('.filters th').index($input.parents('th')),
        $table = $panel.find('.table'),
        $rows = $table.find('tbody tr');
        /* Dirtiest filter function ever ;) */
        var $filteredRows = $rows.filter(function(){
            var value = $(this).find('td').eq(column).text().toLowerCase();
            return value.indexOf(inputContent) === -1;
        });
        /* Clean previous no-result if exist */
        $table.find('tbody .no-result').remove();
        /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
        $rows.show();
        $filteredRows.hide();
        /* Prepend no-result row if all rows are filtered */
        if ($filteredRows.length === $rows.length) {
            $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
        }
    });
});
</script>
@endsection
