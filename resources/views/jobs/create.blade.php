@extends('layout.app')

@section('header')
    @include('include.header')
@endsection
@section('content')
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Post Job</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/jobs">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title" class="col-md-1">Title</label>

                                <div class="col-md-12">
                                    <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" required autofocus>

                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('job_position') ? ' has-error' : '' }}">
                                <label for="job_position" class="col-md-1">Job Position</label>

                                <div class="col-md-12">
                                    <input id="job_position" type="text" class="form-control" name="job_position" value="{{ old('job_position') }}" required autofocus>

                                    @if ($errors->has('job_position'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('job_position') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                                <label for="location" class="col-md-1">Location</label>

                                <div class="col-md-12">
                                    <input id="location" type="text" class="form-control" name="location" value="{{ old('location') }}" required autofocus>

                                    @if ($errors->has('location'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('location') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
                                <label for="company" class="col-md-1">Company</label>

                                <div class="col-md-12">
                                    <input id="company" type="text" class="form-control" name="company" value="{{ old('company') }}" required autofocus>

                                    @if ($errors->has('company'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('company') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('eligibility') ? ' has-error' : '' }}">
                                <label for="eligibility" class="col-md-1">Eligibility</label>

                                <div class="col-md-12">
                                    <input id="eligibility" type="text" class="form-control" name="eligibility" value="{{ old('eligibility') }}" required autofocus>

                                    @if ($errors->has('eligibility'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('eligibility') }}</strong>
                                        </span>
                                    @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('experience') ? ' has-error' : '' }}">
                            <label for="experience" class="col-md-1">Experience</label>

                            <div class="col-md-12">
                                <input id="experience" type="text" class="form-control" name="experience" value="{{ old('experience') }}" required autofocus>

                                @if ($errors->has('experience'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('experience') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                  <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">

                            <div class="col-md-12">
                                <textarea id="content" class="form-control" name="content" value="{{ old('content') }}"></textarea>

                                @if ($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    Post
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('sidebar')
 @include('include.sidebar')
@endsection

@section('script')
@include('include.tinymce')
 </script>
@endsection
