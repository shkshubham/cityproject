@extends('layout.app')

@section('header')
        <div class="intro-header" style="background-color: rgb(240,95,64)">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>WELCOME</h1>
                        <hr class="small">
                        <h2 class='subheading'>{{ Auth::user()->name}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
<div class="row">


    <div class="col-lg-5">
        @foreach($users as $user)
        <div class="media">
            <a class="pull-left" href="#">
                <img class="media-object dp img-circle" src="/images/{{$user->avatar}}" style="width: 100px;height:100px;">
            </a>
            <div class="media-body">
                <a href="/profile/{{$user->username}}"><h4 class="media-heading">{{$user->name}} <small> {{$user->username}}</small></h4></a>
                <!-- <h5>Software Developer at <a href="http://gridle.in">Gridle.in</a></h5>
                <hr style="margin:8px auto">

                <span class="label label-default">HTML5/CSS3</span>
                <span class="label label-default">jQuery</span>
                <span class="label label-info">CakePHP</span>
                <span class="label label-default">Android</span>
                -->
            </div>
        </div>
        @endforeach
    </div>



</div>

@endsection
