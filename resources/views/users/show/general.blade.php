<div class="col-md-12">
        <h2>{{Auth::user()->name}}<small><a class='btn btn-success pull-right' href='/profile/{{Auth::user()->username}}/edit'>Edit</a></small></h2> 
                <div class=" col-md-12 col-lg-12 ">
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Joining date:</td>
                        <td>{{ Auth::user()->created_at->toFormattedDateString() }}</td>
                      </tr>
                      <tr>
                        <td>Date of Birth</td>
                        <td>{{Auth::user()->dob}}</td>
                      </tr>
                   
                         <tr>
                             <tr>
                        <td>Gender</td>
                        <td>{{Auth::user()->gender}}</td>
                      </tr>
                        <tr>
                        <td>Home Address</td>
                        <td>{{Auth::user()->address}}</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><a href="mailto:{{ Auth::user()->email }}">{{ Auth::user()->email }}</a></td>
                      </tr>
                        <td>Phone Number</td>
                        <td>{{Auth::user()->contact}}
                        </td>
                           
                      </tr>
                     
                    </tbody>
                  </table>
                
        </div>
	</div>