
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
                  <table class="table table-user-information">
                             <small><a class='btn btn-success pull-right' href='/profile/{{Auth::user()->username}}/education/edit'>Edit</a></small>
                    <tbody>
                    <h3>10th</h3>
                    <hr>
                      <tr>
                        <td>School Name</td>
                        <td>{{ $usereducation->school_10 }}</td>
                      </tr>
                      <tr>
                        <td>Location</td>
                        <td>{{ $usereducation->location_10 }}</td>
                      </tr>
                   
                         <tr>
                             <tr>
                        <td>Year Completed</td>
                        <td>{{ $usereducation->year_completed_10 }}</td>
                      </tr>
                        <tr>
                        <td>Aggregate</td>
                        <td>{{ $usereducation->aggregate_10 }}</td>
                      </tr>
                      <tr>
                        <td>Board</td>
                        <td>{{ $usereducation->board_10 }}</a></td>
                      </tr>
                        <td>Grade</td>
                        <td>{{ $usereducation->grade_10 }}</td>
                           
                      </tr>
                     
                    </tbody>
                  </table>
            
                <table class="table table-user-information">
                    <tbody>
                    <h3>12th</h3>
                <hr>
                      <tr>
                        <td>School Name</td>
                        <td>{{ $usereducation->school_12 }}</td>
                      </tr>
                      <tr>
                        <td>Location</td>
                        <td>{{ $usereducation->location_12 }}</td>
                      </tr>
                   
                         <tr>
                             <tr>
                        <td>Year Completed</td>
                        <td>{{ $usereducation->year_completed_12 }}</td>
                      </tr>
                        <tr>
                        <td>Aggregate</td>
                        <td>{{ $usereducation->aggregate_12 }}</td>
                      </tr>
                      <tr>
                        <td>Board</td>
                        <td>{{ $usereducation->board_12 }}</a></td>
                      </tr>
                        <td>Grade</td>
                        <td>{{ $usereducation->grade_12 }}</td>
                           
                      </tr>
                     
                    </tbody>
                  </table>
                <table class="table table-user-information">
                    <tbody>
                    <h3>Graduation</h3>
                <hr>
                      <tr>
                        <td>Course Type</td>
                        <td>{{ $usereducation->grad_course_type }}</td>
                      </tr>
                      <tr>
                        <td>Degree</td>
                        <td>{{ $usereducation->grad_degree }}</td>
                      </tr>
                   
                         <tr>
                             <tr>
                        <td>University</td>
                        <td>{{ $usereducation->grad_university }}</td>
                      </tr>
                        <tr>
                        <td>College</td>
                        <td>{{ $usereducation->grad_college }}</td>
                      </tr>
                      <tr>
                        <td>Specialization</td>
                        <td>{{ $usereducation->grad_specialization }}</a></td>
                      </tr>
                        <td>Grade</td>
                        <td>{{ $usereducation->grade_10 }}</td>
                           
                      </tr>
                        <tr>
                        <td>Specialization</td>
                        <td>{{ $usereducation->grad_specialization }}</a></td>
                      </tr>
                        <td>Start Date</td>
                        <td>{{ $usereducation->grad_start_date }}</td>
                           
                      </tr>
                      <tr>
                        <td>End Date</td>
                        <td>{{ $usereducation->grad_end_date }}</a></td>
                      </tr>
                        <td>Prefinal Aggrigate</td>
                        <td>{{ $usereducation->grad_prefinal_aggrigate }}</td>
                           
                      </tr>
                        </tr>
                        <td>Final Aggrigate</td>
                        <td>{{ $usereducation->grad_final_aggrigate }}</td>
                           
                      </tr>
                        </tr>
                        <td>Grade</td>
                        <td>{{ $usereducation->grad_grade }}</td>
                           
                      </tr>
                     
                    </tbody>
                  </table>
                
        </div>
