@extends('layout.app')

@section('header')
@include('include.header')

@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Enter Education Details</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/profile/{{Auth::user()->username}}/education/update">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <h2>10th</h2>
                        <hr>

                        <div class="form-group{{ $errors->has('school_10') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">School Name</label>

                            <div class="col-md-6">
                                <input id="school_10" type="text" value='{{ $usereducation->school_10 }}' class="form-control" name="school_10" value="{{ old('$school_10') }}" required autofocus>

                                @if ($errors->has('school_10'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('school_10') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('location_10') ? ' has-error' : '' }}">
                            <label for="location_10" class="col-md-4 control-label">Location</label>

                            <div class="col-md-6">
                                <input id="location_10" type="text" value='{{ $usereducation->location_10 }}' class="form-control" name="location_10" value="{{ old('location_10') }}" required autofocus>

                                @if ($errors->has('location_10'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('location_10') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('year_completed_10') ? ' has-error' : '' }}">
                            <label for="year_completed_10" class="col-md-4 control-label">Year Completed</label>

                            <div class="col-md-6">
                                <input id="year_completed_10" type="text" value='{{ $usereducation->year_completed_10 }}' class="form-control" name="year_completed_10" value="{{ old('year_completed_10') }}" required autofocus>

                                @if ($errors->has('year_completed_10'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('year_completed_10') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('board_10') ? ' has-error' : '' }}">
                            <label for="board_10" class="col-md-4 control-label">Board</label>

                            <div class="col-md-6">
                                <input id="board_10" type="text" value='{{ $usereducation->board_10 }}' class="form-control" name="board_10" value="{{ old('board_10') }}" required autofocus>

                                @if ($errors->has('board_10'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('board_10') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('aggregate_10') ? ' has-error' : '' }}">
                            <label for="aggregate_10" class="col-md-4 control-label">Aggregate in %</label>

                            <div class="col-md-6">
                                <input id="aggregate_10" type="text" value='{{ $usereducation->aggregate_10 }}' class="form-control" name="aggregate_10" value="{{ old('aggregate_10') }}" required autofocus>

                                @if ($errors->has('aggregate_10'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('aggregate_10') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('grade_10') ? ' has-error' : '' }}">
                            <label for="grade_10" class="col-md-4 control-label">Grade</label>

                            <div class="col-md-6">
                                <input id="grade_10" type="text" value='{{ $usereducation->grade_10 }}' class="form-control" name="grade_10" value="{{ old('grade_10') }}" required autofocus>

                                @if ($errors->has('grade_10'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('grade_10') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                    <h2>12th</h2>
                        <hr>

                        <div class="form-group{{ $errors->has('school_12') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">School Name</label>

                            <div class="col-md-6">
                                <input id="school_12" type="text" value='{{ $usereducation->school_12 }}'  class="form-control" name="school_12" value="{{ old('school_12') }}" required autofocus>

                                @if ($errors->has('school_12'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('school_12') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('location_12') ? ' has-error' : '' }}">
                            <label for="location_12" class="col-md-4 control-label">Location</label>

                            <div class="col-md-6">
                                <input id="location_12" type="text" value='{{ $usereducation->location_12 }}' class="form-control" name="location_12" value="{{ old('location_12') }}" required autofocus>

                                @if ($errors->has('location_12'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('location_12') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('year_completed_12') ? ' has-error' : '' }}">
                            <label for="year_completed_12" class="col-md-4 control-label">Year Completed</label>

                            <div class="col-md-6">
                                <input id="year_completed_12" type="text" value='{{ $usereducation->year_completed_12 }}' class="form-control" name="year_completed_12" value="{{ old('year_completed_12') }}" required autofocus>

                                @if ($errors->has('year_completed_12'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('year_completed_12') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('board_12') ? ' has-error' : '' }}">
                            <label for="board_12" class="col-md-4 control-label">Board</label>

                            <div class="col-md-6">
                                <input id="board_12" type="text" value='{{ $usereducation->board_12 }}' class="form-control" name="board_12" value="{{ old('board_12') }}" required autofocus>

                                @if ($errors->has('board_12'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('board_12') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('aggregate_12') ? ' has-error' : '' }}">
                            <label for="aggregate_12" class="col-md-4 control-label">Aggregate in %</label>

                            <div class="col-md-6">
                                <input id="aggregate_12" type="text" value='{{ $usereducation->aggregate_12 }}' class="form-control" name="aggregate_12" value="{{ old('aggregate_12') }}" required autofocus>

                                @if ($errors->has('aggregate_12'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('aggregate_12') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('grade_12') ? ' has-error' : '' }}">
                            <label for="grade_12" class="col-md-4 control-label">Grade</label>

                            <div class="col-md-6">
                                <input id="grade_12" type="text" value='{{ $usereducation->grade_12 }}' class="form-control" name="grade_12" value="{{ old('grade_12') }}" required autofocus>

                                @if ($errors->has('grade_12'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('grade_12') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

              <h2>Graduation</h2>
                        <hr>

                        <div class="form-group{{ $errors->has('grad_course_type') ? ' has-error' : '' }}">
                            <label for="grad_course_type" class="col-md-4 control-label">Course Type</label>

                            <div class="col-md-6">
                                <input id="grad_course_type" type="text" value='{{ $usereducation->grad_course_type }}' class="form-control" name="grad_course_type" value="{{ old('grad_course_type') }}" required autofocus>

                                @if ($errors->has('grad_course_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('grad_course_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('grad_degree') ? ' has-error' : '' }}">
                            <label for="grad_degree" class="col-md-4 control-label">Degree</label>

                            <div class="col-md-6">
                                <input id="grad_degree" type="text" value='{{ $usereducation->grad_degree }}' value='{{ $usereducation->grad_course_type }}' class="form-control" name="grad_degree" value="{{ old('grad_degree') }}" required autofocus>

                                @if ($errors->has('grad_degree'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('grad_degree') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('grad_university') ? ' has-error' : '' }}">
                            <label for="grad_university" class="col-md-4 control-label">University</label>

                            <div class="col-md-6">
                                <input id="grad_university" type="text" value='{{ $usereducation->grad_university }}' class="form-control" name="grad_university" value="{{ old('grad_universitys') }}" required autofocus>

                                @if ($errors->has('grad_university'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('grad_university') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('grad_college') ? ' has-error' : '' }}">
                            <label for="grad_college" class="col-md-4 control-label">College</label>

                            <div class="col-md-6">
                                <input id="grad_college" type="text" value='{{ $usereducation->grad_college }}' class="form-control" name="grad_college" value="{{ old('grad_college') }}" required autofocus>

                                @if ($errors->has('grad_college'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('grad_college') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('grad_specialization') ? ' has-error' : '' }}">
                            <label for="grad_specialization" class="col-md-4 control-label">Specialization</label>

                            <div class="col-md-6">
                                <input id="grad_specialization" type="text" value='{{ $usereducation->grad_specialization }}' class="form-control" name="grad_specialization" value="{{ old('grad_specialization') }}" required autofocus>

                                @if ($errors->has('grad_specialization'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('grad_specialization') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('grad_start_date') ? ' has-error' : '' }}">
                            <label for="grad_start_date" class="col-md-4 control-label">Start Date</label>

                            <div class="col-md-6">
                                <input id="grad_start_date" type="date" value='{{ $usereducation->grad_start_date }}' class="form-control" name="grad_start_date" value="{{ old('grad_start_date') }}" required autofocus>

                                @if ($errors->has('grad_start_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('grad_start_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('grad_end_date') ? ' has-error' : '' }}">
                            <label for="grad_end_date" class="col-md-4 control-label">End Date</label>

                            <div class="col-md-6">
                                <input id="grad_end_date" type="date" value='{{ $usereducation->grad_end_date }}' class="form-control" name="grad_end_date" value="{{ old('grad_end_date') }}" required autofocus>

                                @if ($errors->has('grad_end_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('grad_end_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('grad_prefinal_aggrigate') ? ' has-error' : '' }}">
                            <label for="grad_prefinal_aggrigate" class="col-md-4 control-label">Prefinal Aggrigate</label>

                            <div class="col-md-6">
                                <input id="grad_prefinal_aggrigate" type="text" value='{{ $usereducation->grad_prefinal_aggrigate }}' class="form-control" name="grad_prefinal_aggrigate" value="{{ old('grad_prefinal_aggrigate') }}" required autofocus>

                                @if ($errors->has('grad_prefinal_aggrigate'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('grad_prefinal_aggrigate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('grad_final_aggrigate') ? ' has-error' : '' }}">
                            <label for="grad_final_aggrigate" class="col-md-4 control-label">Final Aggrigate</label>

                            <div class="col-md-6">
                                <input id="grad_final_aggrigate" type="text" value='{{ $usereducation->grad_final_aggrigate }}' class="form-control" name="grad_final_aggrigate" value="{{ old('grad_final_aggrigate') }}" required autofocus>

                                @if ($errors->has('grad_final_aggrigate'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('grad_final_aggrigate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('grad_grade') ? ' has-error' : '' }}">
                            <label for="grad_grade" class="col-md-4 control-label">Grade</label>

                            <div class="col-md-6">
                                <input id="grad_grade" type="text"  value='{{ $usereducation->grad_grade }}' class="form-control" name="grad_grade" value="{{ old('grad_grade') }}" required autofocus>

                                @if ($errors->has('grad_grade'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('grad_grade') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Edit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
