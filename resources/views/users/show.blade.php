@extends('layout.app')

@section('header')
        <div class="intro-header" style="background-color: rgb(240,95,64)">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>WELCOME</h1>
                        <hr class="small">
                        <h2 class='subheading'>{{ $user->name}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
#upload-file-selector {
    display:none;
}
/*  bhoechie tab */
div.bhoechie-tab-container{
  z-index: 10;
  background-color: #ffffff;
  padding: 0 !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  border:1px solid #ddd;
  margin-top: 20px;
  -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  box-shadow: 0 6px 12px rgba(0,0,0,.175);
  -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  background-clip: padding-box;
  opacity: 0.97;
  filter: alpha(opacity=97);
}
div.bhoechie-tab-menu{
  padding-right: 0;
  padding-left: 0;
  padding-bottom: 0;
}
div.bhoechie-tab-menu ul.list-group{
  margin-bottom: 0;
  list-style:none;
}
div.bhoechie-tab-menu ul.list-group>a{
  margin-bottom: 0;
  text-align:left;
}
div.bhoechie-tab-menu ul.list-group>a .glyphicon,
div.bhoechie-tab-menu ul.list-group>a .fa {
  color: #00001a;
}
div.bhoechie-tab-menu ul.list-group>a:first-child{
  border-top-right-radius: 0;
  -moz-border-top-right-radius: 0;
}
div.bhoechie-tab-menu ul.list-group>a:last-child{
  border-bottom-right-radius: 0;
  -moz-border-bottom-right-radius: 0;
}
div.bhoechie-tab-menu ul.list-group>a.active,
div.bhoechie-tab-menu ul.list-group>a.active .glyphicon,
div.bhoechie-tab-menu ul.list-group>a.active .fa{
  background-color: #004c99;
  background-image: #5A55A3;
  color: #ffffff;
}
div.bhoechie-tab-menu ul.list-group>a.active:after{
  content: '';
  position: absolute;
  left: 100%;
  top: 50%;
  margin-top: -13px;
  border-left: 0;
  border-bottom: 13px solid transparent;
  border-top: 13px solid transparent;
  border-left: 10px solid #5A55A3;
}

div.bhoechie-tab-content{
  background-color: #ffffff;
  /* border: 1px solid #eeeeee; */
  padding-left: 20px;
  padding-top: 10px;
}

div.bhoechie-tab div.bhoechie-tab-content:not(.active){
  display: none;
}
@endsection

@section('content')

<div class="container">

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 bhoechie-tab-menu">
              <ul class="list-group">
                <a href="#" class="list-group-item active">
                  <br/><br/><i class="glyphicon glyphicon-home"></i> Home<br/><br/>
                  </a>
                <a href="#" class="list-group-item ">
                  <br/><br/><i class="glyphicon glyphicon-tasks"></i> Profile<br/><br/>
                </a>
                <a href="#" class="list-group-item ">
                  <br/><br/><i class="glyphicon glyphicon-transfer"></i> Education<br/><br/>
                </a>
              </ul>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 bhoechie-tab">
                <!-- flight section -->
                <div class="bhoechie-tab-content active">
                    <center>
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " align="center">
                    <h2>{{$user->name}}</h2>
                    <h5>{{$user->username}}</h5>
                    <img alt="User Pic" src="/images/{{$user->avatar}}" class="img-circle img-responsive">
                </div>
                    </center>
                </div>


                <div class="bhoechie-tab-content">
                    <center>
                      <div class="col-md-12">
        <h2>{{$user->name}}</h2>
                <div class=" col-md-12 col-lg-12 ">
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Joining date:</td>
                        <td>{{ $user->created_at->toFormattedDateString() }}</td>
                      </tr>
                      <tr>
                        <td>Date of Birth</td>
                        <td>{{$user->dob}}</td>
                      </tr>

                         <tr>
                             <tr>
                        <td>Gender</td>
                        <td>{{$user->gender}}</td>
                      </tr>
                        <tr>
                        <td>Home Address</td>
                        <td>{{$user->address}}</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                      </tr>
                    </tbody>
                  </table>

        </div>
	</div>
                    </center>
                </div>


                <div class="bhoechie-tab-content">
                    <center>
                    @if($usereducation)
                      @include('users.show.education')
                    @else
                    <p>No Education Detail Given By User</p>
                    @endif
                    </center>
                </div>

            </div>
        </div>
  </div>


@endsection
@section('script')
$(document).ready(function() {
    $("div.bhoechie-tab-menu>ul.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});
@endsection
