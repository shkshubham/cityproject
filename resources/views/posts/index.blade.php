@extends('layout.app')

@section('title')

{{ 'Discussion' . ' |'}}
@endsection


@section('style')
.filterable {
    margin-top: 15px;
}
.filterable .panel-heading .pull-right {
    margin-top: -20px;
}
.filterable .filters input[disabled] {
    background-color: transparent;
    border: none;
    cursor: auto;
    box-shadow: none;
    padding: 0;
    height: auto;
}
.filterable .filters input[disabled]::-webkit-input-placeholder {
    color: #333;
}
.filterable .filters input[disabled]::-moz-placeholder {
    color: #333;
}
.filterable .filters input[disabled]:-ms-input-placeholder {
    color: #333;
}


@endsection

@section('header')
    <div class="intro-header" style="background-color: rgb(240,95,64)">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1>DISCUSSION</h1>
                        <hr class="small">
                        <span class="subheading">CityWalkIn</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('content')
<div class="col-lg-12 col-md-12">
    <a href='/posts/create' class='btn'>Start Discussion</a>

       <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <h3 class="panel-title">Menu</h3>
                <div class="pull-right">
                    <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                </div>
            </div>
            <div class="table-responsive">
            <table class="table table-hover table-bordered table-striped">
                <thead>
                    <tr class="filters">
                        <th><input type="text" class="form-control" placeholder="Date Posted" disabled></th>
                        <th><input type="text" class="form-control" placeholder="User" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Category" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Title" disabled></th>
                        <th><input type="text" class="form-control" placeholder="More Information" disabled></th>

                    </tr>
                </thead>
                <tbody>
                 @foreach($posts as $post)
                    <tr>
                        <td>{{ $post->created_at->toFormattedDateString() }}</td>
                        <td>{{ App\Models\User::find($post->user_id)->name}} </td>
                        <td>{{ $post->tags }}</a></td>
                        <td>{{ $post->title }} </td>
                        <td><a href="/posts/{{ $post->slug}}/" class="btn btn-primary btn-lg" role="button">Detail</a></td>
                    </tr>
                    @endforeach
                </tbody>
                     </table>
                     </div>
        </div>
              {{ $posts->links() }}

    </div>


</div>

@endsection

@section('script')
$(document).ready(function(){
    $('.filterable .btn-filter').click(function(){
        var $panel = $(this).parents('.filterable'),
        $filters = $panel.find('.filters input'),
        $tbody = $panel.find('.table tbody');
        if ($filters.prop('disabled') == true) {
            $filters.prop('disabled', false);
            $filters.first().focus();
        } else {
            $filters.val('').prop('disabled', true);
            $tbody.find('.no-result').remove();
            $tbody.find('tr').show();
        }
    });

    $('.filterable .filters input').keyup(function(e){
        /* Ignore tab key */
        var code = e.keyCode || e.which;
        if (code == '9') return;
        /* Useful DOM data and selectors */
        var $input = $(this),
        inputContent = $input.val().toLowerCase(),
        $panel = $input.parents('.filterable'),
        column = $panel.find('.filters th').index($input.parents('th')),
        $table = $panel.find('.table'),
        $rows = $table.find('tbody tr');
        /* Dirtiest filter function ever ;) */
        var $filteredRows = $rows.filter(function(){
            var value = $(this).find('td').eq(column).text().toLowerCase();
            return value.indexOf(inputContent) === -1;
        });
        /* Clean previous no-result if exist */
        $table.find('tbody .no-result').remove();
        /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
        $rows.show();
        $filteredRows.hide();
        /* Prepend no-result row if all rows are filtered */
        if ($filteredRows.length === $rows.length) {
            $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
        }
    });
});
@endsection
