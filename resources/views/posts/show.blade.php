@extends('layout.app')

@section('title')

{{ $post->title . ' |'}}
@endsection

@section('style')
.avatar{
    height:20px;
    width:20px;
}
@endsection
@section('header')
<div class="intro-header" style="background-color: rgb(240,95,64)">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="post-heading">
                        <h6 class="subheading">{{ $post->title }}</h6>
                        <span class="meta">Posted by {{ App\Models\User::find($post->user_id)->name}}  on {{ $post->created_at->toFormattedDateString() }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('content')
@include('include.adsense')
<div class="col-lg-8 col-md-8">

<h4 class="heading">{{ $post->title }}</h4>
<p>Tags:
    <span class="label label-info">{{ $post->tags }}</span>
	| <img class='avatar' alt="User Pic" src="/images/{{ App\Models\User::find($post->user_id)->avatar}}" class="img-circle img-responsive"><i class="icon-user"></i><a href='/profile/{{ App\Models\User::find($post->user_id)->username}}'>{{ App\Models\User::find($post->user_id)->name}}</a>
	| <i class="icon-calendar"></i>  {{ $post->created_at->toFormattedDateString() }} | @if (Auth::user()->id == $post->user_id)
<a href='/posts/{{$post->slug}}/edit'>Edit Post</a>
@endif
</p>
<hr>

<p>{!! nl2br($post->content) !!}</p>
<div class="addthis_inline_share_toolbox"></div>
@include('include.adsense.postbelow')
<hr>

<div id="disqus_thread"></div>
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = '//citywalkin.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58b4b0086f0edfac"></script>



</div>
@endsection


@section('sidebar')
 @include('include.sidebar')
@endsection
