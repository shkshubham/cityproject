@extends('layout.app')

@section('header')
@include('include.header')

@endsection
@section('content')
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Edit</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/posts/{{$post->slug }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input id="title" value='{{ $post->title }}' type="text" class="form-control" name="title" value="{{ old('title') }}" placeholder='Title' required autofocus>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                  <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <textarea id="content" class="form-control" name="content" >{{ $post->content }} </textarea>

                                @if ($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input id="tags" value='{{ $post->tags }}' type="text" class="form-control" name="tags" value="{{ old('tags') }}" placeholder='Tags' required>

                                @if ($errors->has('tags'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tags') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    Post
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection


@section('sidebar')
 @include('include.sidebar')
@endsection

@section('script')
@include('include.tinymce')
 </script>
@endsection