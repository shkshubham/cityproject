@extends('layout.app')

@section('header')
    <div class="intro-header"style="background-color: rgb(240,95,64)">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1>DISCUSSION</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('content')
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Start Discussion</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/posts">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" placeholder='Title' required autofocus>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                  <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <textarea id="content" class="form-control" name="content" value="{{ old('content') }}"></textarea>

                                @if ($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input id="tags" type="text" class="form-control" name="tags" placeholder='Tags' value="{{ old('tags') }}" required>

                                @if ($errors->has('tags'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tags') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    Post
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

@endsection
@section('sidebar')
 @include('include.sidebar')
@endsection

@section('script')
@include('include.tinymce')
 </script>
@endsection