                               <!-- Side Widget Well -->
                <div class="well">
                               <ul class="list-unstyled">

                <ul class="list-unstyled clear-margins"><!-- widgets -->

                    <li class="widget-container widget_nav_menu"><!-- widgets list -->

                        <h1 class="title-widget">RECENT JOBS POST</h1>

                    <ul>
                    @foreach($GlobalJob as $job)
 				        <li><a href="/jobs/{{ $job->slug}}"><i class="fa fa-angle-double-right"></i> {{ $job->title}} </a></li>

                        @endforeach
                     </ul>
					</li>
                </ul>
                <a href="/jobs" class='read  btn'>View All</a>
                </div>



                               <!-- Side Widget Well -->
                <div class="well">
                        <h1 class="title-widget">Search</h1>
                        <form class="form-horizontal" role="form" action='/search' method='GET'>
                        <select class="form-control" name="cat">
                                        <option name'job' value="Jobs" selected>Jobs</option>
                                        <option value="Study">Study</option>
                                        <option value="Discussion">Discussion</option>
                                    </select>
                        <input type='text' name='q' class="form-control" placeholder='Search Study Posts'>
                        <button type="submit" class="btn btn-primary">
                                    Search
                                </button>
                        </form>
                </div>




                                               <!-- Side Widget Well -->
                <div class="well">
                <ul class="list-unstyled">

                <ul class="list-unstyled clear-margins"><!-- widgets -->

                    <li class="widget-container widget_nav_menu"><!-- widgets list -->

                        <h1 class="title-widget">rECENT STUDY POSTS</h1>

                    <ul>
                    @foreach($GlobalStudy as $study)
 				        <li><a href="/study/{{ $study->slug}}"><i class="fa fa-angle-double-right"></i> {{ $study->title}} </a></li>

                        @endforeach
                     </ul>
					</li>
                </ul>
                               <a href="/study" class='read  btn'>View All</a>
                </div>



               <div class="well">
                <ul class="list-unstyled">

                <ul class="list-unstyled clear-margins"><!-- widgets -->

                    <li class="widget-container widget_nav_menu"><!-- widgets list -->

                        <h1 class="title-widget">RECENT Discussion</h1>

                    <ul>
                    @foreach($GlobalPost as $post)
 				        <li><a href="/posts/{{ $post->slug}}"><i class="fa fa-angle-double-right"></i> {{ $post->title}} </a> - {{ App\Models\User::find($post->user_id)->name}}</li>

                        @endforeach
                     </ul>
					</li>
                </ul>

                <a href="/posts" class='read  btn'>View All</a>
                </div>


                <!-- Blog Categories Well -->
                <div class="well">
                    <ul class="list-unstyled">

                <ul class="list-unstyled clear-margins"><!-- widgets -->

                    <li class="widget-container widget_nav_menu"><!-- widgets list -->

                        <a href='/study'><h1 class="title-widget">Study</h1></a>

                    <ul>
 				        <li><a href="/study/category/Verbal/"><i class="fa fa-angle-double-right"></i> Verbal </a></li>
				        <li><a href="/study/category/Quantitative/"><i class="fa fa-angle-double-right"></i> Quantitative </a></li>
				        <li><a href="/study/category/Reasoning/"><i class="fa fa-angle-double-right"></i> Reasoning </a></li>
                        <li><a href="/study/category/Technical/"><i class="fa fa-angle-double-right"></i> Technical Skills </a></li>
                     </ul>

					</li>

                </ul>
                </div>
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Sidebar -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:600px"
     data-ad-client="ca-pub-5405157603871625"
     data-ad-slot="5113330395"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
@section('style')
.read{
    margin-top:20px;
}
@endsection
