<!-- jQuery -->
<script src="/theme/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/theme/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="/theme/js/jqBootstrapValidation.js"></script>
<script src="/theme/js/contact_me.js"></script>

<!-- Theme JavaScript -->
<script src="/theme/js/clean-blog.min.js"></script>


<script src="/creative/vendor/scrollreveal/scrollreveal.min.js"></script>
<script src="/creative/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Theme JavaScript -->
<script src="/creative/js/creative.min.js"></script>
