<!--footer-->
<footer class="footer1">
<div class="container">

<div class="row"><!-- row -->
            
                <div class="col-lg-3 col-md-3"><!-- widgets column left -->
                <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                        	<li class="widget-container widget_nav_menu"><!-- widgets list -->
                    
                                <h1 class="title-widget">Links</h1>
                                
                                <ul>
                                    <li><a  href="/about"><i class="fa fa-angle-double-right"></i> About Us</a></li>
                                    <li><a  href="/contact"><i class="fa fa-angle-double-right"></i> Contact Us</a></li>    
                                </ul>
                    
							</li>
                            
                        </ul>
                         
                      
                </div><!-- widgets column left end -->
                
                
                
                <div class="col-lg-3 col-md-3"><!-- widgets column left -->
            
                <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                        	<li class="widget-container widget_nav_menu"><!-- widgets list -->
                    
                                <h1 class="title-widget">Sections</h1>
                                
                                <ul>
                                        <li><a  href="/posts"><i class="fa fa-angle-double-right"></i>  Posts</a></li>
                                    <li><a  href="/jobs"><i class="fa fa-angle-double-right"></i>  Jobs</a></li>
                                    <li><a  href="/study"><i class="fa fa-angle-double-right"></i>  Study</a></li>
                                
                                </ul>
                    
							</li>
                            
                        </ul>
                         
                      
                </div><!-- widgets column left end -->
                
                
                
                <div class="col-lg-3 col-md-3"><!-- widgets column left -->
            
                <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                        	<li class="widget-container widget_nav_menu"><!-- widgets list -->
                    
                                <h1 class="title-widget">Study</h1>
                                
                                <ul>
 				<li><a href="/study/category/Verbal/"><i class="fa fa-angle-double-right"></i> Verbal </a></li>
				<li><a href="/study/category/Quantitative/"><i class="fa fa-angle-double-right"></i> Quantitative </a></li>
				<li><a href="/study/category/Reasoning/"><i class="fa fa-angle-double-right"></i> Reasoning </a></li>
                                <li><a href="/study/category/Technical/"><i class="fa fa-angle-double-right"></i> Technical Skills </a></li>
                                </ul>
                    
							</li>
                            
                        </ul>
                         
                      
                </div><!-- widgets column left end -->
                
                
                <div class="col-lg-3 col-md-3"><!-- widgets column center -->
                
                   
                    
                        <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                        	<li class="widget-container widget_recent_news"><!-- widgets list -->
                    
                                <h1 class="title-widget">Contact Detail </h1>
                                
                                <div class="footerp"> 
                                
                                <h4 class="title-median">CityWalkIn</h4>
                                <p><b>Email id:</b> <a href="mailto:citywalkin@eutaxy.in">citywalkin@eutaxy.in</a></p>
                                <p><b>Helpline Numbers </b>
    <b style="color:#ffc106;">(8AM to 10PM):</b>  +91-7447667795</p>
                                </div>
                                
                                <div class="social-icons">
                                
                                	<ul class="nomargin">
                                    
                <a href="https://www.facebook.com/citywalkin"><i class="fa fa-facebook-square fa-3x social-fb" id="social"></i></a>
	            <a href="https://twitter.com/citywalkin"><i class="fa fa-twitter-square fa-3x social-tw" id="social"></i></a>
	            <a href="https://plus.google.com/citywalkin"><i class="fa fa-google-plus-square fa-3x social-gp" id="social"></i></a>
	            <a href="mailto:citywalkin@eutaxy.in"><i class="fa fa-envelope-square fa-3x social-em" id="social"></i></a>
                                    
                                    </ul>
                                </div>
                    		</li>
                          </ul>
                       </div>
                </div>
</div>
</footer>
<!--header-->

<div class="footer-bottom">

	<div class="container">

		<div class="row">

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

				<div class="copyright">

					© 2017, CityWalkIn, All rights reserved

				</div>

			</div>

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

				<div class="design">

					 <a href="#">CityWalkIn </a> |  <a target="_blank" href="/">Web Design & Development by Our Team</a>

				</div>

			</div>

		</div>

	</div>

</div>
