<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="/">Citywalkin</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav">
                <li class=" dropdown">
                    <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Jobs <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/jobs/location/Delhi">Delhi</a></li>
                        <li><a href="/jobs/location/Bangalore">Bangalore</a></li>
                        <li><a href="/jobs/location/Hyderabad">Hyderabad</a></li>
                        <li><a href="/jobs/location/Pune">Pune</a></li>
                        <li><a href="/jobs/location/Chennai">Chennai</a></li>
                        <li><a href="/jobs/location/Noida">Noida</a></li>
                        <li><a href="/jobs/location/Mumbai">Mumbai</a></li>
                    </ul>
                </li>
                <li class=" dropdown"><a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Study <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/study/category/Quantitative/">Quantitative</a></li>
                        <li><a href="/study/category/Reasoning/">Reasoning</a></li>
                        <li><a href="/study/category/Verbal/">Verbal</a></li>
                        <li><a href="/study/category/Technical/">Technical Skills</a></li>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a class="page-scroll" href="/about">About</a></li>
                <li><a class="page-scroll" href="/posts">Posts</a></li>
                <li><a class="page-scroll" href="/contact">Contact</a></li>

                @if (Auth::guest())
                    <li><a class="page-scroll" href="{{ route('login') }}">Login</a></li>
                    <li><a class="page-scroll" href="{{ route('register') }}">Register</a></li>
                @else
                    <li class="dropdown"><a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/home">Profile</a></li>
                            <li><a href='/posts/create'>Start Discussion</a></li>
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
